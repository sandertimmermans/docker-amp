#!/bin/bash
SERVERNAME=
DATABASE=
HOST=
PORT=

while getopts 's:d:h:p:' opt; do
    case "${opt}" in
        s) SERVERNAME=$OPTARG ;;
        d) DATABASE=$OPTARG ;;
        h) HOST=$OPTARG ;;
        p) PORT=$OPTARG ;;
        *) error "Unexpected option ${opt}" ;;
    esac
done

cat > /etc/odbcinst.ini <<EOL
[freetds]
Description = MS SQL database access with Free TDS
Driver      = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
Setup       = /usr/lib/x86_64-linux-gnu/odbc/libtdsS.so
UsageCount  = 1
EOL

cat >> /etc/freetds/freetds.conf <<EOL

[$SERVERNAME]
        host = $HOST
        port = $PORT
        tds version = 8.0
EOL

cat > /etc/odbc.ini <<EOL
[$DATABASE]
Description             = MSSQL Server
Driver                  = freetds
ServerName              = $SERVERNAME
Database                = $DATABASE
TDS_Version             = 8.0
EOL