#!/bin/bash
REBUILD=false

while getopts 'r' flag; do
    case "${flag}" in
        r) REBUILD=true ;;
        *) error "Unexpected option ${flag}" ;;
    esac
done

cd "$(dirname "$([ -L "$0" ] && readlink -f "$0" || echo "$0")")" || exit
echo "Directory: $(pwd)"

echo -n "Checking apache-php container ... "
if $REBUILD || ! docker-compose ps | grep -q dockeramp_apache-php
then
    docker-compose build apache-php
fi
echo "done"

echo -n "Checking mysql container      ... "
if $REBUILD || ! docker-compose ps | grep -q dockeramp_mysql
then
    docker-compose build mysql
fi
echo "done"

echo -n "Checking workspace container  ... "
if $REBUILD || ! docker-compose ps | grep -q dockeramp_workspace
then
    docker-compose build workspace
fi
echo "done"

echo "(Re)starting containers ..."
/usr/local/bin/docker-compose down
/usr/local/bin/docker-compose up -d apache-php mysql
echo "Script execution finished."