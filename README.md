# Docker - Apache - MySQL - PHP
## Installation
1. Copy `env_example` to `.env` and configure your preferred settings
2. Configure your apache virtual hosts in `apache-php/sites`
3. Open a terminal in the root folder and run `bash start.sh`
    * Use the `-r` flag if you want to rebuild the containers.
4. When the script completes, run `docker-compose exec workspace bash` to SSH into the workspace container.
## Manual (re)start
1. Open a terminal in the root folder and run `docker-compose down`
2. When the command finishes, run `docker-compose up -d apache-php mysql`
## Connecting to MySQL
To connect to MySQL from the host machine or from applications running in the apache-php container, you will need the MySQL container's IP address as your host parameter (`localhost` will not work), as well as the port if you configured a non-standard port in your `.env` file.

1. Install the containers as described above.
2. Open a terminal in the root folder and run `docker ps | grep mysql`
3. Copy the container ID
3. Run `docker inspect <container id> | grep IPAddress`